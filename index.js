var express = require('express');
var routes = require('./src/routes');
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.json());
    
//Defino el path para acceso publico
app.use(express.static('src/public'));
app.use('/',routes);

app.listen(3000);