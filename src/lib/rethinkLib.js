//Importo "r"
import r from '../../node_modules/rethinkdb/rethinkdb';

/**
 * Crea una conexion con la base de datos
 * @param {json} server 
 */
export async function openConnection(server) {
    try {
        console.log('Conexion exitosa')
        let connection = await r.connect(server)
        return connection
    } catch(err) {
        console.log("Error:", err);
    }
}

/**
 * Cierra una conexion con la base de datos
 */
export async function closeConnection(connection) {
    try {
        await connection.close()
        console.log('Connection closed')
    } catch(err) {
        console.log("Error:", err);
    }
}

/**
 * Crea una base de datos en la conexion establecida
 * @param {*} connection 
 * @param {String} nameDB 
 */
export async function createDB(connection, nameDB){
    try {
        let listDB = await r.dbList().run(connection)
        if( (listDB.find(x => x == nameDB)) == undefined ){
            console.log('Crear DB')
            await r.dbCreate(nameDB).run(connection)
        }else{
            console.log('Ya existe una DB con ese nombre')
        }
    }catch(err){
        console.log("Error:", err)
    }
}

/**
 * Crea una tabla en la base de datos establecida
 * @param {*} connection 
 * @param {String} nameDB 
 * @param {String} nameTable 
 */
export async function createTable(connection, nameDB, nameTable){
    try {
        let listTable = await r.db(nameDB).tableList().run(connection)
        if( (listTable.find(x => x == nameTable)) == undefined ){
            console.log('Crear Tabla')
            await r.db(nameDB).tableCreate(nameTable).run(connection)
        }else{
            console.log('Ya existe una tabla con ese nombre')
        }
    }catch(err){
        console.log("Error:", err)
    }
}

/**
 * Inserta un documento en la tabla especificada
 * @param {*} connection 
 * @param {String} nameDB 
 * @param {String} nameTable 
 * @param {Json} data 
 */
export async function insertDocument(connection, nameDB, nameTable, data){
    try {
        let result = await r.db(nameDB).table(nameTable).insert(data).run(connection);
        console.log(JSON.stringify(result, null, 2));
    } catch (err) {
        console.log("Error: ",err);
    }
}

/**
 * Muestra todos los documentos de una tabla
 * @param {*} connection 
 * @param {String} nameDB 
 * @param {String} nameTable 
 */
export async function showData(connection, nameDB, nameTable) {
    try {
        // La DB retona un cursor
        let cursor = await r.db(nameDB).table(nameTable).run(connection);
        // .toArray() itera sobre los documentos y los agrega en un array
        let result = await cursor.toArray();
        // Llamamos a la funcion a ejecutar tras tener los documentos
        return result
    } catch(err) {
        console.log("Error:", err);
    }
}

/**
 * Filtra los documentos en base a la consulta enviada
 * @param {*} connection 
 * @param {String} nameDB 
 * @param {String} nameTable 
 * @param {query} filtro 
 */
export async function filterDocument(connection, nameDB, nameTable, filtro){
    try {
        let cursor = await r.db(nameDB).table(nameTable).filter(filtro).run(connection);
        let result = await cursor.toArray();
        console.log(JSON.stringify(result, null, 2));
        return result;
    } catch (err) {
        console.log("Error: ",err);
    }
}

/**
 * Retorna un documento de forma eficiente usando su key
 * @param {*} connection 
 * @param {String} nameDB 
 * @param {String} nameTable 
 * @param {key} prymaryKey 
 */
export async function getDocumentForKey(connection, nameDB, nameTable, primaryKey){
    try {
        let result = await r.db(nameDB).table(nameTable).get(primaryKey).run(connection);
        return result;
    } catch (err) {
        console.log("Error: ",err);
    }
}

/**
 * 
 * @param {*} connection 
 * @param {String} nameDB 
 * @param {String} nameTable 
 * @param {Json} newField 
 */
export async function updateDocument(connection, nameDB, nameTable, newField){
    try {
        let result = await r.db(nameDB).table(nameTable).update(newField).run(connection);
        console.log(JSON.stringify(result, null, 2));
    } catch (err) {
        console.log("Error: ",err);
    }
}

/**
 * 
 * @param {*} connection 
 * @param {String} nameDB 
 * @param {String} nameTable 
 * @param {Query} filter 
 * @param {Json} newField 
 */
export async function updateFilterDocument(connection, nameDB, nameTable, filter, newField){
    try {
        let result = await r.db(nameDB).table(nameTable).filter(filter).update(newField).run(connection);
        console.log(JSON.stringify(result, null, 2));
    } catch (err) {
        console.log("Error: ",err);
    }
}

/**
 * 
 * @param {*} connection 
 * @param {String} nameDB 
 * @param {String} nameTable 
 * @param {Query} filter 
 */
export async function deleteDocument(connection, nameDB, nameTable, filter){
    try {
        let result = await r.db(nameDB).table(nameTable).filter(filter).delete().run(connection);
        console.log(JSON.stringify(result, null, 2));
    } catch (err) {
        console.log("Error: ",err);
    }
}


//Monitorea en tiempo real los cambios realizados a una tabla
export function realTime(connection, nameDB, nameTable){
    r.db(nameDB).table(nameTable).changes().run(connection, function(err, cursor) {
        if (err) throw err;
        cursor.each(function(err, row) {
            if (err) throw err;
            console.log(JSON.stringify(row, null, 2));
        });
    });
}
