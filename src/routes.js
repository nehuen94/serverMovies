import r from '../../node_modules/rethinkdb/rethinkdb'
import {db_config} from '../config/dbRethink'


import {
    showData,
    filterDocument
} from '../lib/rethinkLib'

var express = require('express');
var router = express.Router();

// Middleware que crea una conexion a la base de datos 
router.use(createConnection);

// Defino las rutas principales
router.get('/mensaje',showMessage);
router.get('/movies',showMovies);


// Filtros

router.get('/movies/accion',filterAccion);
router.get('/movies/drama',filterDrama);
router.get('/movies/terror',filterTerror);

// Middleware para cerrar la conexion a la base de datos
router.use(closeConnection);

module.exports = router;

/*
 * Crea una conexion a RethinkDB y lo salva en req.connection
 */
function createConnection(req, res, next) {
    r.connect(db_config.rethink).then(function(conn) {
        req.connection = conn;
        next();
    }).error(handleError(res));
}

/*
 * Cierra una conexion de RethinkDB
 */
function closeConnection(req, res, next) {
    req.connection.close();
}

/*
 * Send back a 500 error
 */
function handleError(res) {
    return function(error) {
        res.send(500, {error: error.message});
    }
}

/*
* Muestra mensaje
*/
function showMessage(req, res, next){
    res.send('Server OK')
}

/*
* Muestra todo el contenido de la tabla movies
*/
function showMovies(req, res, next){
    showData(req.connection,'moviesdb','movies')
    .then((data)=>{
        res.send(data)
    })
}

/**
 * Filtra por categoria
*/
function filterAccion(req,res,next){
    filterDocument(req.connection,'moviesdb','movies',r.row("categoria").eq("accion"))
    .then((data)=>{
        res.send(data)
    })
}

function filterDrama(req,res,next){
    filterDocument(req.connection,'moviesdb','movies',r.row("categoria").eq("drama"))
    .then((data)=>{
        res.send(data)
    })
}

function filterTerror(req,res,next){
    filterDocument(req.connection,'moviesdb','movies',r.row("categoria").eq("terror"))
    .then((data)=>{
        res.send(data)
    })
}


